#!/usr/bin/python3
import pandas as pd 
import numpy as np
import scipy
import sklearn
import sklearn.model_selection
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from tkinter import *
from sklearn.metrics import mean_squared_error

### 3D
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D

### system
import sys
var1 = sys.argv[1]
var2 = sys.argv[2]
var3 = sys.argv[3]

df = pd.read_excel(var1,sheet_name = var2)
X = np.array([df["X_in"],df["X_out"]])
X = X.T
Y = np.array(df['y'])


print(" All X data ")
print(" Length = " +str(len(X)))
print(X)
print("\n\n")
print(" All Y Data ")
print(" Length = " +str(len(Y)))
print(Y)

X_train, X_test, Y_train, Y_test = sklearn.model_selection.train_test_split(X,Y,test_size = float(var3), random_state =0)
#X_train = X[:865]
#Y_train = Y[:865]
#X_test = X[866:]
#Y_test = Y[866:]


print(" X_train' length = "+ str(len(X_train)) + "\n")
print(" X_test' length = "+ str(len(X_test)) + "\n")

reg = LinearRegression().fit(X_train,Y_train)
score = reg.score(X_train,Y_train)
coef = reg.coef_
intercept = reg.intercept_

print(" traning score = "+ str(score))
print(" coef = " + str(coef))
print(" intercept = " + str(intercept))
print(" EQ : y = "+ str(intercept) +" + "+ str(coef[0])+'x1 + ' + str(coef[1])+'x2')

Y_test_pred = reg.predict(X_test)
Y_train_pred = reg.predict(X_train)
score2 = reg.score(X_test,Y_test)
print(" test score = "+str(score2))

lin_mse_train = mean_squared_error(Y_train_pred, Y_train)
lin_mse_test = mean_squared_error(Y_test_pred, Y_test)
lin_rmse_train = np.sqrt(lin_mse_train)
lin_rmse_test = np.sqrt(lin_mse_test)
print('Linear Regression RMSE of trainSet: %.4f' %lin_rmse_train)
print('Linear Regression RMSE of testSet : %.4f' %lin_rmse_test)


########### Scatter 2D
#plt.scatter(Y_test, Y_test_pred)
#plt.xlabel(" Y_test ")
#plt.ylabel(" predicted data")
#plt.title(" Y_test VS predicted Y ")
#plt.show()


#plt.scatter(Y_train, Y_train_pred)
#plt.xlabel(" Y_train")
#plt.ylabel(" predicted data")
#plt.title(" Y_train VS predicted Y")
#plt.show()
################################

######### 3D graph

#mpl.rcParams['legend.fontsize'] = 10
#fig = plt.figure()
#ax = fig.gca(projection='3d')
#ax = fig.add_subplot(111,projection='3d')
#z = Y
#x1 = df["X_in"]
#x2 = df["X_out"]

#ax.scatter(x1,x2,z)
#ax.set_xlabel(" X in ")
#ax.set_ylabel(" X out ")
#ax.set_zlabel(" Y ")

#ax.plot_trisurf(x1,x2,z,linewidth=0.2,antialiased=True)
#ax.plot(x1,x2,z,label='graph')
#ax.legend()
#plt.show()
#######################################
